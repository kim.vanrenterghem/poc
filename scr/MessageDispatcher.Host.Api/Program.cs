﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using MessageDispatcher.Api;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using Microsoft.Extensions.Configuration;

namespace MessageDispatcher.Host.Api
{
    public class Program : Aplication
    {
        private static IWebHost WebHost { get; set; }
        private static bool isService;

        public static void Main(string[] args)
        {
            bool isOk = CheckForRequiredArgs(args);

            if (isOk)
            {
                Console.WriteLine("-----------------------------------------------------------");
                Console.WriteLine("----------------- Argument Validation OK ------------------");
                Console.WriteLine("-----------------------------------------------------------");
                WebHost = CreateWebHostBuilder(args).Build();

                if (isService)
                {
                    WebHost.RunAsService();
                }
                else
                {
                    WebHost.Run();
                }
            }
            else
            {
                Console.WriteLine("-----------------------------------------------------------");
                Console.WriteLine("--------------- Argument Validation NOT OK ----------------");
                Console.WriteLine("-----------------------------------------------------------");
            }
        }

        private static bool CheckForRequiredArgs(string[] args)
        {
            ArgsValidation argsValidation = new ArgsValidation();
            Console.WriteLine("-----------------------------------------------------------\n" +
                              "------------------- Argument Validation -------------------\n" +
                              "-----------------------------------------------------------\n\n");
            argsValidation.Mongo = ValidateArg("mongoconnection", args);
            argsValidation.Kafla = ValidateArg("kafkaconnection", args);
            argsValidation.ActiveMQ = ValidateArg("amqconnection", args);
            argsValidation.Sql = ValidateArg("sqlconnection", args);
            Console.WriteLine("\n-----------------------------------------------------------\n" +
                              "--------------- End of Argument Validation ----------------\n" +
                              "-----------------------------------------------------------\n\n");
            return argsValidation.Valid;
        }

        private static bool ValidateArg(string argName, string[] args)
        {
            if (args.FirstOrDefault(x => x.Contains(argName)) != null)
            {
                Console.WriteLine($"{argName} is validated successfully!");
                return true;
            }
            else
            {
                Console.WriteLine($"{argName} Could not be found!");
                return false;
            }
        }

        public void Stop()
            => WebHost.Dispose();

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            isService = !(Debugger.IsAttached || args.Contains("--console"));
            var pathToExe = Process.GetCurrentProcess().MainModule.FileName;
            var pathToContentRoot = Path.GetDirectoryName(pathToExe);

            var conf = new ConfigurationBuilder()
                .AddCommandLine(args)
                .Build();

            var builder = Microsoft.AspNetCore.WebHost.CreateDefaultBuilder(args)
                .UseApplicationInsights()
                .UseUrls("http://*:500")
                .UseConfiguration(conf)
                .UseStartup<Startup>();

            if (isService)
            {
                builder.UseContentRoot(pathToContentRoot);
            }

            return builder;
        }
    }
}