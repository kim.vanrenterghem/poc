﻿using MessageDispatcher.Api.Broker.AMQ;
using MessageDispatcher.Api.Broker.Kafka;
using Microsoft.Extensions.Configuration;

namespace MessageDispatcher.Host.Api
{
    public class Configuratie : IActiveMqConnection, IKafkaConnection
    {
        private IConfiguration _configuration;

        public Configuratie(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string ActiveMqConnection
            => _configuration.GetValue<string>("amqconnection");

        public string KafkaConnection
            => _configuration.GetValue<string>("kafkaconnection");
    }
}