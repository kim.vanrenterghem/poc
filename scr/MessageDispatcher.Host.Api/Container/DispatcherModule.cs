﻿using Autofac;
using MessageDispatcher.Api;
using MessageDispatcher.Api.Broker;
using MessageDispatcher.Api.Broker.AMQ;
using MessageDispatcher.Api.Broker.Kafka;
using MessageDispatcher.Api.Dispatcher;
using MessageDispatcher.Api.Dispatcher.Hangfire;

namespace MessageDispatcher.Host.Api.Container
{
    public class DispatcherModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<ActiveMqSession>()
                .AsSelf();
                //.InstancePerDependency()
                //.OnRelease(i => i.Dispose());

            builder
                .RegisterType<ActiveMqSender>()
                .As<ICommandSender>()
                .AsSelf();

            builder
                .RegisterType<KafkaSender>()
                .As<IEventSender>()
                .AsSelf();

            builder
                .RegisterType<ObjectSerializer>()
                .As<ISerializer>();

            builder
                .RegisterType<EventScheduler>()
                .As<IEventDispatcher>();
            builder
                .RegisterType<CommandScheduler>()
                .As<ICommandDispatcher>();
        }

    }
}