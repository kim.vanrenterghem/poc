﻿using Autofac;

namespace MessageDispatcher.Host.Api.Container
{
    public class ContainerBuilderFactory
    {
        public ContainerBuilder Create()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule(new ConfigurationModule());
            containerBuilder.RegisterModule(new DispatcherModule());
            containerBuilder.RegisterModule(new ProgramModule());

            return containerBuilder;
        }

    }
}