﻿using Autofac;
using MessageDispatcher.Api;

namespace MessageDispatcher.Host.Api.Container
{
    public class ProgramModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<Program>()
                .As<Aplication>();
        }

    }
}