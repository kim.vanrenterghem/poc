﻿using Autofac;
using MessageDispatcher.Api.Broker.AMQ;
using MessageDispatcher.Api.Broker.Kafka;

namespace MessageDispatcher.Host.Api.Container
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<Configuratie>()
                .As<IActiveMqConnection>()
                .As<IKafkaConnection>()
                .SingleInstance();
        }

    }
}