﻿namespace MessageDispatcher.Host.Api
{
    public class ArgsValidation
    {
        public bool Mongo { get; set; }
        public bool Kafla { get; set; }
        public bool ActiveMQ { get; set; }
        public bool Sql { get; set; }

        public bool Valid => Kafla && ActiveMQ && (Mongo || Sql);
    }
}