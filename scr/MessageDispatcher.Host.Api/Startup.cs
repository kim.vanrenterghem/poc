﻿using System;
using Autofac.Extensions.DependencyInjection;
using Hangfire;
using Hangfire.Mongo;
using MessageDispatcher.Host.Api.Container;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace MessageDispatcher.Host.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            if (Configuration.GetValue<string>("mongoconnection") != null)
            {
                services.AddHangfire(x => x.UseMongoStorage(Configuration.GetValue<string>("mongoconnection"), "HangFire"));
            } else if (Configuration.GetValue<string>("sqlconnection") != null)
            {
                services.AddHangfire(x => x.UseSqlServerStorage(Configuration.GetValue<string>("sqlconnection")));
            }
            
            services.AddMvc()
                .AddControllersAsServices()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            
            services
                .AddSwaggerGen(c =>{c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });});

            var containerBuilder = new ContainerBuilderFactory()
                .Create();

            containerBuilder.Populate(services);
            var container = containerBuilder.Build();
            return new AutofacServiceProvider(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            var options = new BackgroundJobServerOptions { WorkerCount = 100 };
            app.UseHangfireServer(options);
            app.UseHangfireDashboard();

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>{c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");});
        }
    }
}