﻿using Newtonsoft.Json;

namespace MessageDispatcher.Api
{
    public class ObjectSerializer : ISerializer
    {
        public string Serialize(object obj) => JsonConvert.SerializeObject(obj);
    }
}