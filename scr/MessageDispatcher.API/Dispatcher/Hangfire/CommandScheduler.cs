﻿using Hangfire;
using MessageDispatcher.Api.Broker;
using MessageDispatcher.Api.Models;

namespace MessageDispatcher.Api.Dispatcher.Hangfire
{
    public class CommandScheduler : ICommandDispatcher
    {
        private readonly ICommandSender _commandSender;

        public CommandScheduler(ICommandSender commandSender)
        {
            _commandSender = commandSender;
        }

        public void Dispatch(string destination, string command, DispatchMessage message)
        {
            BackgroundJob.Enqueue(() => _commandSender.Send(destination, command, message));
        }
    }
}
