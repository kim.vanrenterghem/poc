using System.Threading.Tasks;
using MessageDispatcher.Api.Broker;
using MessageDispatcher.Api.Models;

namespace MessageDispatcher.Api.Dispatcher.StraightForward
{
    public class EventScheduler: IEventDispatcher
    {

        private readonly IEventSender _eventSender;

        public EventScheduler(IEventSender eventSender)
        {
            _eventSender = eventSender;
        }

        public void Dispatch(string destination, string command, DispatchMessage message)
        {
            Task.Run(() => _eventSender.Send(destination, command, message));
        }
    }
}
