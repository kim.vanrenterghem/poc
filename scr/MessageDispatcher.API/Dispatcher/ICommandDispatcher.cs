﻿using MessageDispatcher.Api.Models;

namespace MessageDispatcher.Api.Dispatcher
{
    public interface ICommandDispatcher
    {
        void Dispatch(string streamName, string messageName, DispatchMessage message);
    }
}
