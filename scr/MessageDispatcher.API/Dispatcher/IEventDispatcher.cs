﻿using MessageDispatcher.Api.Models;

namespace MessageDispatcher.Api.Dispatcher
{
    public interface IEventDispatcher
    {
        void Dispatch(string streamName, string messageName, DispatchMessage message);
    }
}
