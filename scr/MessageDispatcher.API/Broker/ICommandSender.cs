﻿using MessageDispatcher.Api.Models;

namespace MessageDispatcher.Api.Broker
{
    public interface ICommandSender
    {
        void Send(string streamName, string messageName, DispatchMessage dispatchMessage);
    }
}