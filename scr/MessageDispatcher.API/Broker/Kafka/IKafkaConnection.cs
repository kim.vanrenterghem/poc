﻿namespace MessageDispatcher.Api.Broker.Kafka
{
    public interface IKafkaConnection
    {
        string KafkaConnection { get; }
    }
}
