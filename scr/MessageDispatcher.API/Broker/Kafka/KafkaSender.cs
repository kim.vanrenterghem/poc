﻿using System;
using System.Collections.Generic;
using System.Text;
using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using MessageDispatcher.Api.Models;
using MessageDispatcher.Api.Tracking;

namespace MessageDispatcher.Api.Broker.Kafka
{
    public class KafkaSender : ApplicationInsights, IEventSender
    {
        private readonly string _kafkaconnection;
        private readonly Func<DispatchMessage, string> _serializer;

        public KafkaSender(IKafkaConnection configuration,ISerializer serializer)
        {
            _kafkaconnection = configuration.KafkaConnection;
            _serializer = serializer.Serialize;
        }

        public void Send(string streamName, string messageName, DispatchMessage dispatchMessage)
        {
            try
            {
                var producerConfig = new Dictionary<string, object> { { "bootstrap.servers", _kafkaconnection } };
                using (var producer = new Producer<Null, string>(producerConfig, null, new StringSerializer(Encoding.UTF8)))
                {
                    var json = _serializer(dispatchMessage);
                    producer.ProduceAsync(streamName, null, json).Wait();
                }
            }
            catch (Exception e)
            {

                ForwardException(e);
            }
        }
    }
}
