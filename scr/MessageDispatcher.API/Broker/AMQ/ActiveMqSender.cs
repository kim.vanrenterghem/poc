﻿using System;
using Amqp;
using MessageDispatcher.Api.Models;
using MessageDispatcher.Api.Tracking;

namespace MessageDispatcher.Api.Broker.AMQ
{
    public class ActiveMqSender : ApplicationInsights, ICommandSender
    {
        private readonly ActiveMqSession _session;
        private readonly Func<DispatchMessage, string> _serializer;

        public ActiveMqSender(ISerializer serializer, ActiveMqSession session)
        {
            _session = session;
            _serializer = serializer.Serialize;
        }
        public void Send(string streamName, string messageName, DispatchMessage dispatchMessage)
        {
            try
            {
                var senderLink = new SenderLink(_session.Session, messageName, streamName);

                var json = _serializer(dispatchMessage);
                senderLink.Send(new Message(json));

                senderLink.Close();
            }
            catch (Exception e)
            {
                ForwardException(e);
            }
        }
    }
}