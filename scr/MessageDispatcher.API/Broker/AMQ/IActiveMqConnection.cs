﻿namespace MessageDispatcher.Api.Broker.AMQ
{
    public interface IActiveMqConnection
    {
        string ActiveMqConnection { get; }
    }
}