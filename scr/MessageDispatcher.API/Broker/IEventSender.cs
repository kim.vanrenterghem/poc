﻿using MessageDispatcher.Api.Models;

namespace MessageDispatcher.Api.Broker
{
    public interface IEventSender
    {
        void Send(string streamName, string messageName, DispatchMessage dispatchMessage);
    }
}