﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MessageDispatcher.Api.Models
{
    public class DispatchMessage
    {
        public DispatchMessage()
        {
            ReceiveDateTime = DateTime.UtcNow;
            RequestId = Guid.NewGuid().ToString();
        }
        
        public string MessageName { get; set; }
        [Required]
        public string Header { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        public string EntityID { get; set; }
        public DateTime ReceiveDateTime { get; }
        public string RequestId { get; set; }
    }
}