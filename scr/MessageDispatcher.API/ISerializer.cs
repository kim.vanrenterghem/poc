﻿namespace MessageDispatcher.Api
{
    public interface ISerializer
    {
        string Serialize(object obj);
    }
}