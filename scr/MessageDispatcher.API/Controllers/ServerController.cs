﻿using Microsoft.AspNetCore.Mvc;

namespace MessageDispatcher.Api.Controllers
{
    [Route("api/server")]
    public class ServerController : Controller
    {
        private readonly Aplication _aplication;

        public ServerController(Aplication aplication)
        {
            _aplication = aplication;
        }
        [HttpPost("stop/")]
        public void Stop()
            => _aplication.Stop();
    }
}