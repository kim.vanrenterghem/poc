﻿using Microsoft.ApplicationInsights;
using System;

namespace MessageDispatcher.Api.Tracking
{
    public abstract class ApplicationInsights
    {
        private TelemetryClient telemetry = new TelemetryClient();

        public ApplicationInsights()
        {
               
        }

        public void ForwardException(Exception exception)
        {
            telemetry.TrackException(exception);
            throw exception;
        }

    }
}
