﻿using System;
using Amqp;

namespace MessageDispatcher.Api.Broker.AMQ
{
    public class ActiveMqSession : IDisposable
    {
        private Connection _connection;
        private readonly string _activeMqConnectionString;
        private Session _session;

        public ActiveMqSession(IActiveMqConnection configuration)
        {
            _activeMqConnectionString = configuration.ActiveMqConnection;
        }


        public Session Session
        {
            get
            {
                if (_session == null)
                {
                    var address = new Address(_activeMqConnectionString);
                    _connection = new Connection(address);
                    _session = new Session(_connection);
                }

                return _session;
            }
        }

        public void Dispose()
        {
            _session.Close();
            _connection.Close();
        }
    }
}