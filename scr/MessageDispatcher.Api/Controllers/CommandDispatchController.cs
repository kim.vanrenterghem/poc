﻿using System;
using MessageDispatcher.Api.Dispatcher;
using MessageDispatcher.Api.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MessageDispatcher.Api.Controllers
{
    [Route("api/commandDispatch")]
    public class CommandDispatchController : Controller
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public CommandDispatchController(ICommandDispatcher commandDispatcher)
        {
            _commandDispatcher = commandDispatcher;
        }

        [HttpPost("{destination}/{command}")]
        public IActionResult Command(string destination, string command, [FromBody] DispatchMessage message)
        {
            message.MessageName = command;
            try
            {
                _commandDispatcher.Dispatch(destination, command, message);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }

}
