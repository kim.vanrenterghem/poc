﻿using System;
using MessageDispatcher.Api.Dispatcher;
using MessageDispatcher.Api.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MessageDispatcher.Api.Controllers
{
    [Route("api/eventDispatch")]
    public class EventDispatchController : Controller
    {
        private readonly IEventDispatcher _eventDispatcher;

        public EventDispatchController(IEventDispatcher eventDispatcher)
        {
            _eventDispatcher = eventDispatcher;
        }

        [HttpPost("{source}/{event}")]
        public IActionResult Event( string source, string @event, [FromBody] DispatchMessage message)
        {
            message.MessageName = @event;
            try
            {
                _eventDispatcher.Dispatch(source, @event, message);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }

}
