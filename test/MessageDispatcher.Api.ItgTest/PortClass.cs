﻿namespace MessageDispatcher.Api.ItgTest
{
    public class PortClass
    {
        public int Id { get; set; }
        public int MongoPort1 { get; set; }
        public int MongoPort2 { get; set; }
        public int SqlPort1 { get; set; }
        public int AppPort1 { get; set; }
        public int KafkaPort1 { get; set; }
        public int KafkaPort2 { get; set; }
        public int ActiveMqPort1 { get; set; }
        public int ActiveMqPort2 { get; set; }
        public string Db { get; set; }
    }
}
