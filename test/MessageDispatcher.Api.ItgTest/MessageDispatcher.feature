﻿Feature: MessageDispatcher
	validating thad all messages are forwarded as they should
	even if the broker (activeMq or/and Kafka) is unavailable and the server is restarted


	@stopAllProcesses
Scenario: A Command is send to ActiveMQ
	Given the dispatcher is running
	And the ActiveMq is running
	When sending 1 Command
	Then 1 Command is received

	@stopAllProcesses
Scenario: An Event is send to Kafka
	Given the dispatcher is running
	And the Kafka is running
	When sending 1 Event
	Then wait for 20 seconds so hangfire can process
	Then 1 Event is received

	@stopAllProcesses
Scenario: 1000 Commands are sended to ActiveMQ
	Given the dispatcher is running
	And the ActiveMq is running
	When sending 1000 Command
	Then 1000 Command is received

	@stopAllProcesses
Scenario: 1000 Events are sended to Kafka
	Given the dispatcher is running
	And the Kafka is running
	When sending 1000 Event
	Then wait for 20 seconds so hangfire can process
	Then 1000 Event is received

#	@stopAllProcesses
#Scenario: 10.000 Commands are sended to ActiveMQ
#	Given the dispatcher is running
#	And the ActiveMq is running
#	When sending 10000 Command
#	Then 10000 Command is received
#
#	@stopAllProcesses
#Scenario: 10.000 Events are sended to Kafka
#	Given the dispatcher is running
#	And the Kafka is running
#	When sending 10000 Event
#	Then wait for 20 seconds so hangfire can process
#	Then 10000 Event is received

	@stopAllProcesses
Scenario: A Command is send to ActiveMQ while broker is down
	Given the dispatcher is running
	When sending 1 Command
	Then the ActiveMQ starts
	And 1 Command is received

	@stopAllProcesses
Scenario: An Event is send to Kafka while broker is down
	Given the dispatcher is running
	When sending 1 Event
	Then the Kafka starts
	Then wait for 10 seconds so hangfire can process
	Then 1 Event is received

	@stopAllProcesses
Scenario: A Command is send to ActiveMQ while broker is down and dispatcher restarted
	Given the dispatcher is running
	When sending 1 Command
	Then the dispatcher stops
	Then the dispatcher starts
	Then the ActiveMQ starts
	Then 1 Command is received

	@stopAllProcesses
Scenario: An Event is send to Kafka while broker is down and dispatcher restarted
	Given the dispatcher is running
	When sending 1 Event
	Then the dispatcher stops
	Then the dispatcher starts
	Then the Kafka starts
	Then wait for 10 seconds so hangfire can process
	Then 1 Event is received
