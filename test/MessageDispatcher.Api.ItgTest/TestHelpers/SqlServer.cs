﻿namespace MessageDispatcher.Api.ItgTest.TestHelpers
{
    public static class SqlServer
    {
        public static void Start(PortClass portClass)
        {
            Command.Run("docker",
                $"  run -e \"ACCEPT_EULA=Y\" -e \"SA_PASSWORD=<YourStrong!Passw0rd>\" -p {portClass.SqlPort1}:1433 --name db-node{portClass.Id}  mcr.microsoft.com/mssql/server:latest",
                ".* Service Broker manager has started.");

            Command.RunWithResult("docker",
                $" exec db-node{portClass.Id} /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P \"<YourStrong!Passw0rd>\" -Q \"CREATE DATABASE Hangfire\"");
        }

        public static void Stop(PortClass portClass)
        {
            Command.RunWithResult("docker", $"rm db-node{portClass.Id} -f");
        }
    }
}
