﻿using Amqp;

namespace MessageDispatcher.Api.ItgTest.TestHelpers
{
    public static class ActiveMq
    {
        public static void Start(PortClass portClass)
        {
            Command.Run("docker",
                $" run --name activemq-node{portClass.Id} -p {portClass.ActiveMqPort1}:5672 -p {portClass.ActiveMqPort2}:8161 rmohr/activemq",
                "Apache ActiveMQ .* started");
        }
        public static void Stop(PortClass portClass)
        {
            Command.RunWithResult("docker", $"rm activemq-node{portClass.Id} -f");
        }

        public static bool CheckStart(PortClass portClass)
        {
            return Command.RunWithResult("docker", "inspect -f {{.State.Running}} activemq-node" + portClass.Id) == "true\n";
        }

        public static Address GetAddress(PortClass portClass)
        {
            return new Address(ClientUrl(portClass));
        }

        private static string ClientUrl(PortClass portClass)
        {
            return $"amqp://admin:admin@localhost:{portClass.ActiveMqPort1}";
        }

        

    }
}
