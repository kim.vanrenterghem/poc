﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace MessageDispatcher.Api.ItgTest.TestHelpers
{
    public static class Command
    {
        public static string RunWithResult(string fileName, string arguments)
        {
            var process = SetupRun(fileName, arguments);

            process.Start();
            return process.StandardOutput.ReadToEnd();
        }
        public static void Run(string fileName, string arguments, string success)
        {
            var messageDispatcherStarted = new AutoResetEvent(false);
            var process = SetupRun(fileName, arguments);

            process.OutputDataReceived += OnOutputDataReceived;
            process.ErrorDataReceived += OnOutputDataReceived;

            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            messageDispatcherStarted.WaitOne(TimeSpan.FromSeconds(20));

            void OnOutputDataReceived(object sender, DataReceivedEventArgs e)
            {
                if (!string.IsNullOrEmpty(e.Data) && Regex.IsMatch(e.Data, success))
                {
                    messageDispatcherStarted.Set();
                }
            }
        }

        private static Process SetupRun(string fileName, string arguments)
        {
            var startInfo =
                new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = fileName,
                    Arguments = arguments,
                    CreateNoWindow = true,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false
                };

            var process = new Process
            {
                StartInfo = startInfo
            };
            return process;
        }
    }
}
