﻿namespace MessageDispatcher.Api.ItgTest.TestHelpers
{
    public static class Kafka
    {
        public static void Start(PortClass portClass)
        {
            Command.Run("docker",
                $" run --name kafka-node{portClass.Id} -p {portClass.KafkaPort1}:2181 -p {portClass.KafkaPort2}:9092 --env ADVERTISED_HOST=localhost --env ADVERTISED_PORT=9092 spotify/kafka",
                ".*success: kafka entered RUNNING state.*");
        }

        public static void Stop(PortClass portClass)
        {
            Command.RunWithResult("docker", $"rm kafka-node{portClass.Id} -f");
        }

        public static bool CheckStart(PortClass portClass)
        {
            return Command.RunWithResult("docker", "inspect -f {{.State.Running}} kafka-node" + portClass.Id) == "true\n";
        }
    }
}
