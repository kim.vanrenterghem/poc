﻿using System;
using MessageDispatcher.Api.Client;

namespace MessageDispatcher.Api.ItgTest.TestHelpers
{
    public static class Dispatcher
    {
        private static void StartWithMongo(PortClass portClass)
        {
            Command.Run("cmd.exe",
                "/K cd ..\\..\\..\\..\\..\\scr\\MessageDispatcher.Host.Api\\ &" +
                " dotnet build &" +
                $" dotnet run --urls=http://*:{portClass.AppPort1} mongoconnection=mongodb://localhost:{portClass.MongoPort1} kafkaconnection=localhost:{portClass.KafkaPort2} amqconnection=amqp://admin:admin@localhost:{portClass.ActiveMqPort1} --console",
                "Application started.*");
        }

        private static void StartWithSql(PortClass portClass)
        {
            Command.Run("cmd.exe",
                "/K cd ..\\..\\..\\..\\..\\scr\\MessageDispatcher.Host.Api\\ &" +
                " dotnet build &" +
                $" dotnet run --urls=http://*:{portClass.AppPort1} sqlconnection=\"Data Source=127.0.0.1,{portClass.SqlPort1};Initial Catalog=Hangfire;Persist Security Info=True;User ID=SA;Password=<YourStrong!Passw0rd>;\" kafkaconnection=localhost:{portClass.KafkaPort2} amqconnection=amqp://admin:admin@localhost:{portClass.ActiveMqPort1} --console",
                "Application started.*");
        }

        public static void StartDispatcher(PortClass portClass)
        {
            switch (portClass.Db)
            {
                case "Mongo":
                    StartWithMongo(portClass);
                    break;
                case "Sql":
                    StartWithSql(portClass);
                    break;
            }
        }

        public static CommandDispatchClient GetCommandDispatcher(PortClass portClass)
        {
            return new CommandDispatchClient(ClientUrl(portClass));
        }

        public static EventDispatchClient GetEventDispatcher(PortClass portClass)
        {
            return new EventDispatchClient(ClientUrl(portClass));
        }

        public static ServerClient GetServerClient(PortClass portClass)
        {
            return new ServerClient(ClientUrl(portClass));
        }

        private static string ClientUrl(PortClass portClass)
        {
            return $"http://localhost:{portClass.AppPort1}";
        }

        public static void Stop(PortClass portClass)
        {
            var client = GetServerClient(portClass);
            try
            {
                client.StopAsync().Wait();
            }
            catch (Exception e)
            {
                if (e.InnerException?.InnerException != null &&
                    (e.InnerException.InnerException.Message != "The underlying connection was closed: The connection was closed unexpectedly."
                     && e.InnerException.InnerException.Message != "De onderliggende verbinding is gesloten: De verbinding is onverwachts afgesloten."))
                    throw;
            }
        }
    }
}