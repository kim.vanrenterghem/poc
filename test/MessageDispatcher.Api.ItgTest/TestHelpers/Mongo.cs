﻿namespace MessageDispatcher.Api.ItgTest.TestHelpers
{
    public static class Mongo
    {
        public static void Start(PortClass portClass)
        {
            Command.Run("docker",
                $" run --name db-node{portClass.Id} -p {portClass.MongoPort1}:27017 -p {portClass.MongoPort1}:28017 mongo:latest",
                "*build index done.*");
        }

        public static void Stop(PortClass portClass)
        {
            Command.RunWithResult("docker", $"rm db-node{portClass.Id} -f");
        }
    }
}
