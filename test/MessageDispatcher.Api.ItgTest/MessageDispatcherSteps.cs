﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Amqp;
using Confluent.Kafka;
using FluentAssertions;
using MessageDispatcher.Api.Client;
using MessageDispatcher.Api.ItgTest.TestHelpers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using TechTalk.SpecFlow;

namespace MessageDispatcher.Api.ItgTest
{
    [Binding]
    public class MessageDispatcherSteps
    {
        public bool IsDatabaseRunning { get; set; }
        public bool IsAMQRunning { get; set; }
        public bool IsKafkaRunning { get; set; }
        public PortClass _portClass { get;}

        public MessageDispatcherSteps()
        {
            var config =
                new ConfigurationBuilder()
                    .AddJsonFile("AppSettings.json")
                    .Build();

            var id = Int32.Parse(config["TestId"]);
            _portClass = new PortClass
            {
                Id = id,
                Db = "Sql",
                //Db = "Mongo",
                MongoPort1 = 27000 + id,
                MongoPort2 = 28000 + id,
                SqlPort1 = 1400 + id,
                ActiveMqPort1 = 5600 + id,
                ActiveMqPort2 = 8100 + id,
                AppPort1 = 5500 + id,
                KafkaPort1 = 2100 + id,
                KafkaPort2 = 9000 + id
            };
        }


        [Given(@"the dispatcher is running")]
        public void GivenTheDispatcherIsRunning()
        {
            IsDatabaseRunning = Command.RunWithResult("docker", "inspect -f {{.State.Running}} db-node" + _portClass.Id) == "true\n";

            if (!IsDatabaseRunning)
            {
                switch (_portClass.Db)
                {
                    case "Mongo":
                        Mongo.Start(_portClass);
                        break;
                    case "Sql":
                        SqlServer.Start(_portClass);
                        break;
                }
            }
            Dispatcher.StartDispatcher(_portClass);
        }

        [Given(@"the ActiveMq is running")]
        [Then(@"the ActiveMQ starts")]
        public void GivenTheActiveMqIsRunning()
        {
            IsAMQRunning = ActiveMq.CheckStart(_portClass);

            if (!IsAMQRunning)
                ActiveMq.Start(_portClass);
        }

        [Given(@"the Kafka is running")]
        [Then(@"the Kafka starts")]
        public void ThenTheKafkaStarts()
        {
            IsKafkaRunning = Kafka.CheckStart(_portClass);
            if (!IsKafkaRunning)
                Kafka.Start(_portClass);
        }

        [When(@"sending (.*) Command")]
        public void WhenSendingMessage(int numberOfMessages)
        {
            var client = Dispatcher.GetCommandDispatcher(_portClass);

            Enumerable.Range(0, numberOfMessages)
                .Select(i => new DispatchMessage { Body = $"Body{i}", Header = "Header", EntityID = i.ToString(), RequestId = Guid.NewGuid() })
                .ToList()
                .ForEach(m => client.CommandAsync("DestinationUnknown", "CommandTest", m).Wait());
        }

        [Then(@"(.*) Command is received")]
        public void ThenMessageIsReceived(int numberOfMessages)
        {
            var address = ActiveMq.GetAddress(_portClass);
            var connection = new Connection(address);

            try
            {
                var session = new Session(connection);
                var receiverLink = new ReceiverLink(session, "CommandTest", "DestinationUnknown");

                var amqpMessage = new Amqp.Message();

                var msgCount = 0;
                var messages = new List<DispatchMessage>();
                while ((amqpMessage = receiverLink.Receive(TimeSpan.FromSeconds(100))) != null)
                {
                    var json = amqpMessage.Body.ToString();
                    var message = JsonConvert.DeserializeObject<DispatchMessage>(json);
                    receiverLink.Accept(amqpMessage);
                    msgCount++;
                    messages.Add(message);
                }

                numberOfMessages.Should().Be(msgCount);

                messages = messages.OrderBy(x => x.Body).ToList();

                Enumerable.Range(0, numberOfMessages)
                .Select(i => messages.FirstOrDefault(x => x.Body == $"Body{i}" && x.Header == "Header" && x.MessageName == "CommandTest" && x.EntityID == i.ToString()))
                .ToList()
                .ForEach(m => m.Should().NotBeNull());
            }
            finally
            {
                connection.Close();
            }
        }

        [Then(@"wait for (.*) seconds so hangfire can process")]
        public void WaitForHangfire(double seconds)
        {
            Thread.Sleep((int)seconds * 1000);
        }

        [When(@"sending (.*) Event")]
        public void WhenSendingEvent(int numberOfMessages)
        {
            var client = Dispatcher.GetEventDispatcher(_portClass);

            Enumerable.Range(0, numberOfMessages)
                .Select(i => new DispatchMessage { Body = $"Body{i}", Header = "Header", EntityID = i.ToString(), RequestId = Guid.NewGuid() })
                .ToList()
                .ForEach(m => client.EventAsync("DestinationUnknown", "EventTest", m).Wait());
        }

        [Then(@"(.*) Event is received")]
        public void ReceiveFromKafka(int numberOfMessages)
        {
            var consumerConfig = new ConsumerConfig
            {
                GroupId = Guid.NewGuid().ToString(),
                BootstrapServers = $"localhost:{_portClass.KafkaPort2}",
                SessionTimeoutMs = 6000,
                AutoOffsetReset = AutoOffsetResetType.Earliest,
                EnableAutoCommit = true
            };

            int readMessages = 0;
            List<DispatchMessage> messages = new List<DispatchMessage>();

            using (var consumer = new Consumer<Ignore, string>(consumerConfig))
            {
                bool done = false;
                consumer.OnPartitionEOF += (_, tpo)
                    => done = true;

                consumer.OnPartitionsRevoked += (_, partitions)
                    => consumer.Unassign();

                consumer.Subscribe("DestinationUnknown");

                while (!done)
                {
                    ConsumeResult<Ignore, string> record = consumer.Consume(TimeSpan.FromSeconds(70));
                    if (record != null)
                    {
                        messages.Add(JsonConvert.DeserializeObject<DispatchMessage>(record.Message.Value));
                        readMessages += 1;
                    }
                }
            }

            readMessages.Should().Be(numberOfMessages);

            messages = messages.OrderBy(x => x.Body).ToList();

            Enumerable.Range(0, numberOfMessages)
                .Select(i => messages.FirstOrDefault(x => x.Body == $"Body{i}" && x.Header == "Header" && x.MessageName == "EventTest" && x.EntityID == i.ToString()))
                .ToList()
                .ForEach(m => m.Should().NotBeNull());
        }

        [Then(@"the dispatcher stops")]
        public void ThenTheDispatcherStops()
        {
           Dispatcher.Stop(_portClass);
        }

        [Then(@"the dispatcher starts")]
        public void ThenTheDispatcherStarts()
        {
            Dispatcher.StartDispatcher(_portClass);
        }

        [AfterScenario("stopAllProcesses")]
        public void StopAllProcesses()
        {
            Dispatcher.Stop(_portClass);

            if (!IsAMQRunning)
                ActiveMq.Stop(_portClass);

            if (!IsKafkaRunning)
                Kafka.Stop(_portClass);

            if (!IsDatabaseRunning)
                switch (_portClass.Db)
                {
                    case "Mongo":
                        Mongo.Stop(_portClass);
                        break;
                    case "Sql":
                        SqlServer.Stop(_portClass);
                        break;
                }
            
        }
    }
}