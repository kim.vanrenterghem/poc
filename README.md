# Introduction 
This small app is a buffer between a existing application and a message broker. so that the application can broadcast messages (events / commands ) without failure. 

# Getting Started
1.	For the tests Docker is required. 
1.	docker pull rmohr/activemq
1.  docker pull spotify/kafka
1.  docker 
1.	install the visual studio extention for specflow (tools > extentions > online)
1.  install .NET Core SDK 2.2.100-preview3 or higher 

# Run the api
- Open the \scr\MessageDispatcher.API
- for MONGO: dotnet run --urls=http://*:5582 mongoconnection=mongodb://localhost:27000 kafkaconnection=localhost:9092 amqconnection=amqp://admin:admin@localhost:5672 --console
OR 
- for SQL: dotnet run --urls=http://*:5581 sqlconnection="Data Source=127.0.0.1,1433;Initial Catalog=Hangfire;Persist Security Info=True;User ID=SA;Password=<YourStrong!Passw0rd>" kafkaconnection=localhost:9092 amqconnection=amqp://admin:admin@localhost:5672 --console
    - you can change the port 
    - you can change the http protocol to https

## Swagger api description
http://localhost:5000/swagger

simpel  to read the api interface and to do tests (https://swagger.io/)

## MongoDb in docker
* docker run --name mongo-node -p 27000:27017 -p 28017:28017 mongo:latest
* connect to localhost:27000
* tool : https://studio3t.com/download/

## Sql server in docker
* docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=<YourStrong!Passw0rd>" -p 1433:1433 --name sql1  -d mcr.microsoft.com/mssql/server:latest
* docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P "<YourStrong!Passw0rd>"  -Q "CREATE DATABASE Hangfire"
> Data Source=127.0.0.1,1433;Initial Catalog=Hangfire;Persist Security Info=True;User ID=SA;Password=<YourStrong!Passw0rd>

## Hangfire dashboard 

> Hangfire Depents on a Sql server ore mongo db  instens

> you can switch it in the startup class at ln 29 - 30
```csharp
    //sql server
    services.AddHangfire(x => x.UseSqlServerStorage(Configuration.GetConnectionString("HangfireConnection")));
    //Mongo db
    services.AddHangfire(x => x.UseMongoStorage(Configuration.GetConnectionString("MongodbConnection"), "HangFire"));
```
http://localhost:5000/Hangfire

Hangfire schedules the jobs (https://www.hangfire.io/)


## Spin-up Docker ActiveMQ
* docker pull rmohr/activemq
* docker run --name activemq-node -p 5672:5672 -p 8161:8161 rmohr/activemq
* ActiveMQ: http://localhost:8161/

Manage ActiveMQ : http://localhost:8161/admin/
```
    Login: admin
    Password: admin
```
## Spin-up Docker Kafka
* docker pull spotify/kafka
* docker run --name kafka-node -p 2181:2181 -p 9092:9092 --env ADVERTISED_HOST=localhost --env ADVERTISED_PORT=9092 spotify/kafka
* Kafka Tool: http://www.kafkatool.com/download.html


### Windows Service (MessageDispatcher.Host.Api)
1. execute "dotnet publish --configuration Release --runtime win10-x64 --output c:\svc"
1. execute "net user ServiceUser {PASSWORD} /add"
1. execute "net localgroup Administrators ServiceUser /add"
1. execute "icacls "c:\svc" /grant ServiceUser:(OI)(CI)WRX /t"
1. execute "sc create MessageDispatcher binPath= "c:\svc\MessageDispatcher.Host.Api.exe --urls=http://*:5582 mongoconnection=mongodb://localhost:27000 kafkaconnection=localhost:9092  mqconnection=amqp://admin:admin@localhost:5672" obj= ".\ServiceUser" password="{PASSWORD}""
1. execute "sc start MessageDispatcher"

More info: https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/windows-service?view=aspnetcore-2.1